const axios = require('axios');
const DDB = require('aws-sdk/clients/dynamodb');
const SQS = require('aws-sdk/clients/sqs');

const ddb = new DDB();

const apiKey = process.env.apiKey;
const teamId = process.env.teamId;

const TableName = 'FaceItLog';

const baseUrl = 'https://open.faceit.com/data/v4/';

const teamUrl = (teamId) => `teams/${teamId}`;
const playerUrl = (playerId) => `players/${playerId}`;
const matchUrl = (matchId) => `matches/${matchId}/stats`;

const MATCH_CONFIGURING = 'match_status_configuring';
const MATCH_FINISHED = 'match_status_finished';
const MATCH_CANCELLED = 'match_status_cancelled';
const MATCH_COMPLETE = 'match_complete';

const WARTIDE_MATCH_TYPE = '5v5 WARTIDE';

const buildApiRequest = (endpoint) => axios.get(baseUrl + endpoint, {
    headers: {
        Authorization: 'Bearer ' + apiKey
    }
});

const getTeamMembers = async () => {
    const {data: team} = await buildApiRequest(teamUrl(teamId));
    return team.members.map(({user_id}) => user_id);
};
const getPlayer = async (playerId) => {
    const {data: player} = await buildApiRequest(playerUrl(playerId));
    return player;
};
const getMatchStats = async (matchId) => {
    const {data: match} = await buildApiRequest(matchUrl(matchId));
    return match.rounds[0];
};
const updateMatch = ({game, startTime, playerData, status, map}) => {
    const params = {
        Key: {
            game: {
                S: game
            },
            startTime: {
                N: startTime.toString()
            }
        },
        UpdateExpression: 'SET #pd = :pd, #status = :st, #map = :map',
        ExpressionAttributeValues: {
            ':pd': {
                S: JSON.stringify(playerData)
            },
            ':st': {
                S: status
            },
            ':map': {
                S: map
            }
        },
        ExpressionAttributeNames: {
            "#pd": 'playerData',
            "#status": 'status',
            "#map": 'map'
        },
        TableName
    }
    return ddb.updateItem(params).promise();
}
const addMatch = ({matchId, startTime, playerData, game, status, elos}) => {
    const params = {
        Item: {
            matchId: {
                S: matchId
            },
            startTime: {
                N: startTime.toString()
            },
            playerData: {
                S: JSON.stringify(playerData)
            },
            game: {
                S: game
            },
            players: {
                SS: playerData.map(({name}) => name)
            },
            status: {
                S: status
            },
            elos: {
                S: JSON.stringify(elos)
            },
            season: {
                N: '3'
            }
        },
        TableName
    };
    console.log(params);
    return ddb.putItem(params).promise();
};
const getMatch = async (game, startTime) => {
    const params = {
        Key: {
            game: {
                S: game
            },
            startTime: {
                N: startTime.toString()
            }
        },
        AttributesToGet: [
            'matchId',
            'playerData',
            'players'
        ],
        TableName
    };
    const result = await ddb.getItem(params).promise();
    return result ? {
        matchId: result.Item.matchId.S,
        playerData: JSON.parse(result.Item.playerData.S),
        players: result.Item.players.SS,
        game,
        startTime
    } : undefined;
};
const deleteMatch = (game, startTime) => {
    const params = {
        Key: {
            game: {
                S: game
            },
            startTime: {
                N: startTime.toString()
            }
        },
        TableName
    };
    return ddb.deleteItem(params).promise();
};

const handleEvent = async (event) => {
    if (!event.event || event.event.indexOf('mock') < 0) {
        console.log(JSON.stringify(event));
    }
    if (event.payload.entity.name !== WARTIDE_MATCH_TYPE) {
        // filter out showdown matches
        return;
    }
    const teamMembers = await getTeamMembers();
    const teamPlayers = [];
    const game = event.payload.game;
    const matchId = event.payload.id;
    const startTime = new Date(event.payload.created_at).getTime();

    if (event.event.indexOf(MATCH_FINISHED) >= 0) {
        console.log('handling match finished');
        event.event = MATCH_COMPLETE;
        const params = {
            MessageBody: JSON.stringify(event),
            QueueUrl: 'https://sqs.us-west-2.amazonaws.com/239336166108/FaceitMatchFinishedQueue'
        };
        const sqs = new SQS();
        await sqs.sendMessage(params).promise();
    } else if (event.event.indexOf(MATCH_CANCELLED) >= 0) {
        console.log('handling match cancelled');
        await deleteMatch(game, startTime);
    } else if (event.event.indexOf(MATCH_COMPLETE) >= 0) {
        console.log('handingline match complete');
        const savedMatch = await getMatch(game, startTime);
        if (savedMatch) {
            const matchData = await getMatchStats(savedMatch.matchId);
            const playerData = savedMatch.playerData;
            const matchPlayers = [];
            const map = matchData.round_stats.Map;
            matchData.teams.forEach(({players}) => players.forEach((player) => matchPlayers.push(player)));
            const updatedPlayers = await Promise.all(playerData.map(async (player) => {
                const matchPlayer = matchPlayers.find(({player_id}) => player_id === player.id);
                if (matchPlayer) {
                    player.kd = matchPlayer.player_stats['K/D Ratio'];
                    player.score = matchPlayer.player_stats.Score;
                    player.kills = matchPlayer.player_stats.Kills;
                    player.deaths = matchPlayer.player_stats.Deaths;
                }
                const players = await getPlayer(player.id);
                if (players.games[game]) {
                    player.afterElo = players.games[game].faceit_elo;
                }
                return player;
            }));
            await updateMatch({...savedMatch, playerData: updatedPlayers, status: 'complete', map});
        } else {
            console.log('match not found');
        }
    } else if (event.event.indexOf(MATCH_CONFIGURING) >= 0) {
        console.log('handling match configuring');
        let enemyElo = 0;
        let friendlyElo = 0;
        const playerData = [];
        await Promise.all(event.payload.teams.map(async ({roster}) => {
            const isFriendly = !!roster.find(({id}) => teamMembers.indexOf(id) >= 0);
            await Promise.all(roster.map(async ({id}) => {
                const isTeamPlayer = teamMembers.indexOf(id) >= 0;
                const players = await getPlayer(id);
                console.log(players.games[game].faceit_elo);
                console.log(isFriendly);
                console.log(players.nickname);
                if (players.games[game]) {
                    if (isTeamPlayer) {
                        teamPlayers.push(id);
                        playerData.push({
                            id,
                            name: players.nickname,
                            // skill: players.games[game].skill_level,
                            beforeElo: players.games[game].faceit_elo
                        });
                    }
                    if (isFriendly) {
                        friendlyElo += players.games[game].faceit_elo;
                    } else {
                        enemyElo += players.games[game].faceit_elo;
                    }
                }
            }));
        }));
        await addMatch({game, matchId, playerData, startTime, status: 'starting', elos: {friendlyElo, enemyElo}});
    } else {
        console.log('Event not handled');
        console.log(JSON.stringify(event));
    }
};
exports.handler = async (event) => {
    if (!event.event || event.event.indexOf('mock') < 0) {
        console.log(JSON.stringify(event));
    }
    if (!event.Records) {
        await handleEvent(event);
    } else {
        await Promise.all(event.Records.map(({body}) => handleEvent(JSON.parse(body))));
    }
    return {
        statusCode: 200
    };
};
