const sampleTeamResult = {
    "team_id": "6e6f587e-08ec-4a02-928a-e28c040b7064",
    "nickname": "texna",
    "name": "texna",
    "description": "",
    "avatar": "",
    "cover_image": "",
    "game": "battalion",
    "team_type": "premade",
    "members": [
        {
            "user_id": "2ca53dd0-7d1c-4f31-aa34-9584f00edc45",
            "nickname": "Airtamis",
            "avatar": "",
            "country": "US",
            "membership_type": "free",
            "memberships": [
                "free"
            ],
            "faceit_url": "https://www.faceit.com/{lang}/players/Airtamis"
        },
        {
            "user_id": "604da348-7e74-406e-8187-6b652279fc76",
            "nickname": "Luniix",
            "avatar": "https://assets.faceit-cdn.net/avatars/604da348-7e74-406e-8187-6b652279fc76_1558886428975.jpg",
            "country": "us",
            "membership_type": "free",
            "memberships": [
                "free"
            ],
            "faceit_url": "https://www.faceit.com/{lang}/players/Luniix"
        },
        {
            "user_id": "70f4d7fc-6326-4741-afba-5105d83fb4e1",
            "nickname": "Moonpee",
            "avatar": "",
            "country": "US",
            "membership_type": "free",
            "memberships": [
                "free"
            ],
            "faceit_url": "https://www.faceit.com/{lang}/players/Moonpee"
        },
        {
            "user_id": "94607d1b-7fcf-4db7-a085-50fe8c24f2f2",
            "nickname": "nationwide13",
            "avatar": "",
            "country": "US",
            "membership_type": "premium",
            "memberships": [
                "premium"
            ],
            "faceit_url": "https://www.faceit.com/{lang}/players/nationwide13"
        },
        {
            "user_id": "c8b31b95-e86b-4985-8bbf-e2c14b8b8962",
            "nickname": "SlowBanana",
            "avatar": "https://assets.faceit-cdn.net/avatars/c8b31b95-e86b-4985-8bbf-e2c14b8b8962_1559825747524.jpg",
            "country": "US",
            "membership_type": "premium",
            "memberships": [
                "premium"
            ],
            "faceit_url": "https://www.faceit.com/{lang}/players/SlowBanana"
        },
        {
            "user_id": "d841fc8d-7433-4415-bdab-ab387f9f9e97",
            "nickname": "Dablues",
            "avatar": "https://assets.faceit-cdn.net/avatars/d841fc8d-7433-4415-bdab-ab387f9f9e97_1558886389622.jpg",
            "country": "US",
            "membership_type": "free",
            "memberships": [
                "free"
            ],
            "faceit_url": "https://www.faceit.com/{lang}/players/Dablues"
        }
    ],
    "leader": "94607d1b-7fcf-4db7-a085-50fe8c24f2f2",
    "website": "",
    "facebook": "",
    "twitter": "",
    "youtube": "",
    "chat_room_id": "team-6e6f587e-08ec-4a02-928a-e28c040b7064",
    "faceit_url": "https://www.faceit.com/{lang}/teams/6e6f587e-08ec-4a02-928a-e28c040b7064"
};
const samplePlayerResult = {
    "player_id": "94607d1b-7fcf-4db7-a085-50fe8c24f2f2",
    "nickname": "nationwide13",
    "avatar": "",
    "country": "US",
    "cover_image": "",
    "cover_featured_image": "",
    "infractions": {
        "last_infraction_date": "",
        "afk": 0,
        "leaver": 0,
        "qm_not_checkedin": 0,
        "qm_not_voted": 0
    },
    "platforms": {
        "steam": "STEAM_0:0:33056454"
    },
    "games": {
        "battalion": {
            "game_profile_id": "ba1b957a-2e12-461e-a466-d1122343a584",
            "region": "NA",
            "regions": null,
            "skill_level_label": "3",
            "game_player_id": "76561198026378636",
            "skill_level": 3,
            "faceit_elo": 1045,
            "game_player_name": "nationwide13"
        }
    },
    "settings": {
        "language": "en"
    },
    "friends_ids": [
        "c8b31b95-e86b-4985-8bbf-e2c14b8b8962",
        "d841fc8d-7433-4415-bdab-ab387f9f9e97",
        "604da348-7e74-406e-8187-6b652279fc76",
        "2ca53dd0-7d1c-4f31-aa34-9584f00edc45",
        "70f4d7fc-6326-4741-afba-5105d83fb4e1",
        "9feb9b5c-b568-41f5-8fba-b7204f7615bb",
        "a9a26c02-2686-49bd-a523-d7b88198c6e1"
    ],
    "bans": [],
    "new_steam_id": "[U:1:66112908]",
    "steam_id_64": "76561198026378636",
    "steam_nickname": "nationwide",
    "membership_type": "premium",
    "memberships": [
        "premium"
    ],
    "faceit_url": "https://www.faceit.com/{lang}/players/nationwide13"
};
const sampleMatchResult = {
    "rounds": [
        {
            "best_of": "1",
            "competition_id": null,
            "game_id": "battalion",
            "game_mode": "5v5 WARTIDE",
            "match_id": "1-7457f67f-f4d5-4ae1-be64-15abfbb884eb",
            "match_round": "1",
            "played": "1",
            "round_stats": {
                "Map": "Vanguard",
                "Region": "NA",
                "Winner": "c8b31b95-e86b-4985-8bbf-e2c14b8b8962"
            },
            "teams": [
                {
                    "team_id": "edc24b80-1b89-4895-b5ed-08c2c4166d17",
                    "premade": false,
                    "team_stats": {
                        "Team": "team_Jasonm31178",
                        "Team Win": "0"
                    },
                    "players": [
                        {
                            "player_id": "b458f71e-0c65-4040-b0c7-fd5d84f60103",
                            "nickname": "Draaaake",
                            "player_stats": {
                                "Assists": "3",
                                "Deaths": "20",
                                "K/D Ratio": "1.4",
                                "Kills": "28",
                                "Result": "0",
                                "Score": "154"
                            }
                        },
                        {
                            "player_id": "1e1db879-e9f5-4ada-90ed-66cf7b1e2ab8",
                            "nickname": "TheMagic",
                            "player_stats": {
                                "Assists": "1",
                                "Deaths": "20",
                                "K/D Ratio": "0.85",
                                "Kills": "17",
                                "Result": "0",
                                "Score": "93"
                            }
                        },
                        {
                            "player_id": "137403c6-cb61-46ab-a36e-8be0cb1cf53c",
                            "nickname": "Snorplet",
                            "player_stats": {
                                "Assists": "3",
                                "Deaths": "22",
                                "K/D Ratio": "0.32",
                                "Kills": "7",
                                "Result": "0",
                                "Score": "44"
                            }
                        },
                        {
                            "player_id": "47552eb2-9d81-489e-854f-726f3f0daec2",
                            "nickname": "GonRL",
                            "player_stats": {
                                "Assists": "1",
                                "Deaths": "17",
                                "K/D Ratio": "1.06",
                                "Kills": "18",
                                "Result": "0",
                                "Score": "108"
                            }
                        },
                        {
                            "player_id": "edc24b80-1b89-4895-b5ed-08c2c4166d17",
                            "nickname": "Jasonm31178",
                            "player_stats": {
                                "Assists": "3",
                                "Deaths": "19",
                                "K/D Ratio": "0.68",
                                "Kills": "13",
                                "Result": "0",
                                "Score": "74"
                            }
                        }
                    ]
                },
                {
                    "team_id": "c8b31b95-e86b-4985-8bbf-e2c14b8b8962",
                    "premade": false,
                    "team_stats": {
                        "Team": "team_SlowBanana",
                        "Team Win": "1"
                    },
                    "players": [
                        {
                            "player_id": "d6ab8dd4-442f-4c18-bb5e-c3704348db69",
                            "nickname": "BAMBAM15435",
                            "player_stats": {
                                "Assists": "0",
                                "Deaths": "17",
                                "K/D Ratio": "1.06",
                                "Kills": "18",
                                "Result": "1",
                                "Score": "95"
                            }
                        },
                        {
                            "player_id": "abbc8f50-897d-41f0-9a5d-1fac530e955d",
                            "nickname": "NitrousTV",
                            "player_stats": {
                                "Assists": "7",
                                "Deaths": "17",
                                "K/D Ratio": "0.94",
                                "Kills": "16",
                                "Result": "1",
                                "Score": "116"
                            }
                        },
                        {
                            "player_id": "20b73b21-55dd-4f48-bb59-0b7436630f17",
                            "nickname": "MeoBoi",
                            "player_stats": {
                                "Assists": "2",
                                "Deaths": "18",
                                "K/D Ratio": "1.06",
                                "Kills": "19",
                                "Result": "1",
                                "Score": "101"
                            }
                        },
                        {
                            "player_id": "94607d1b-7fcf-4db7-a085-50fe8c24f2f2",
                            "nickname": "nationwide13",
                            "player_stats": {
                                "Assists": "1",
                                "Deaths": "16",
                                "K/D Ratio": "1.69",
                                "Kills": "27",
                                "Result": "1",
                                "Score": "138"
                            }
                        },
                        {
                            "player_id": "c8b31b95-e86b-4985-8bbf-e2c14b8b8962",
                            "nickname": "SlowBanana",
                            "player_stats": {
                                "Assists": "3",
                                "Deaths": "17",
                                "K/D Ratio": "1.06",
                                "Kills": "18",
                                "Result": "1",
                                "Score": "109"
                            }
                        }
                    ]
                }
            ]
        }
    ]
}
const sampleMatchConfiguringHook = {
    "transaction_id": "3e41a12f-fff2-4f02-83d2-300f50d9d16d",
    "event": "match_status_configuring",
    "event_id": "4f7a3574-f9e6-424e-9fce-dbd4c25d25fc",
    "third_party_id": "94607d1b-7fcf-4db7-a085-50fe8c24f2f2",
    "app_id": "11ad71cf-6172-4ec1-9555-e91163264284",
    "timestamp": "2019-06-12T20:59:51Z",
    "retry_count": 0,
    "version": 1,
    "payload": {
        "id": "1-7457f67f-f4d5-4ae1-be64-15abfbb884eb",
        "organizer_id": "faceit",
        "region": "NA",
        "game": "battalion",
        "version": 15,
        "entity": {
            "id": "36c6f816-f417-4477-9afb-f67dec946191",
            "name": "5v5 WARTIDE",
            "type": "matchmaking"
        },
        "teams": [
            {
                "id": "edc24b80-1b89-4895-b5ed-08c2c4166d17",
                "name": "team_Jasonm31178",
                "type": "",
                "avatar": "",
                "leader_id": "edc24b80-1b89-4895-b5ed-08c2c4166d17",
                "co_leader_id": "",
                "roster": [
                    {
                        "id": "b458f71e-0c65-4040-b0c7-fd5d84f60103",
                        "nickname": "Draaaake",
                        "avatar": "",
                        "game_id": "76561198091687678",
                        "game_name": "Draaaake",
                        "game_skill_level": 3,
                        "membership": "",
                        "anticheat_required": false
                    },
                    {
                        "id": "1e1db879-e9f5-4ada-90ed-66cf7b1e2ab8",
                        "nickname": "TheMagic",
                        "avatar": "https://assets.faceit-cdn.net/avatars/1e1db879-e9f5-4ada-90ed-66cf7b1e2ab8_1553472718614.jpg",
                        "game_id": "76561198036484203",
                        "game_name": "TheMagic",
                        "game_skill_level": 3,
                        "membership": "",
                        "anticheat_required": false
                    },
                    {
                        "id": "137403c6-cb61-46ab-a36e-8be0cb1cf53c",
                        "nickname": "Snorplet",
                        "avatar": "",
                        "game_id": "76561198835785075",
                        "game_name": "Snorplet",
                        "game_skill_level": 3,
                        "membership": "",
                        "anticheat_required": false
                    },
                    {
                        "id": "47552eb2-9d81-489e-854f-726f3f0daec2",
                        "nickname": "GonRL",
                        "avatar": "https://assets.faceit-cdn.net/avatars/47552eb2-9d81-489e-854f-726f3f0daec2_1560303255466.jpg",
                        "game_id": "76561198266490093",
                        "game_name": "GonRL",
                        "game_skill_level": 3,
                        "membership": "",
                        "anticheat_required": false
                    },
                    {
                        "id": "edc24b80-1b89-4895-b5ed-08c2c4166d17",
                        "nickname": "Jasonm31178",
                        "avatar": "",
                        "game_id": "76561198137252986",
                        "game_name": "Jasonm31178",
                        "game_skill_level": 3,
                        "membership": "",
                        "anticheat_required": false
                    }
                ],
                "substitutions": 0,
                "substitutes": null
            },
            {
                "id": "c8b31b95-e86b-4985-8bbf-e2c14b8b8962",
                "name": "team_SlowBanana",
                "type": "",
                "avatar": "https://assets.faceit-cdn.net/avatars/c8b31b95-e86b-4985-8bbf-e2c14b8b8962_1559825747524.jpg",
                "leader_id": "c8b31b95-e86b-4985-8bbf-e2c14b8b8962",
                "co_leader_id": "",
                "roster": [
                    {
                        "id": "d6ab8dd4-442f-4c18-bb5e-c3704348db69",
                        "nickname": "BAMBAM15435",
                        "avatar": "",
                        "game_id": "76561198350193474",
                        "game_name": "BAMBAM15435",
                        "game_skill_level": 3,
                        "membership": "",
                        "anticheat_required": false
                    },
                    {
                        "id": "abbc8f50-897d-41f0-9a5d-1fac530e955d",
                        "nickname": "NitrousTV",
                        "avatar": "https://assets.faceit-cdn.net/avatars/abbc8f50-897d-41f0-9a5d-1fac530e955d_1550834631821.jpg",
                        "game_id": "76561198079633804",
                        "game_name": "NitrousTV",
                        "game_skill_level": 3,
                        "membership": "",
                        "anticheat_required": false
                    },
                    {
                        "id": "20b73b21-55dd-4f48-bb59-0b7436630f17",
                        "nickname": "MeoBoi",
                        "avatar": "",
                        "game_id": "76561198001474191",
                        "game_name": "MeoBoi",
                        "game_skill_level": 3,
                        "membership": "",
                        "anticheat_required": false
                    },
                    {
                        "id": "94607d1b-7fcf-4db7-a085-50fe8c24f2f2",
                        "nickname": "nationwide13",
                        "avatar": "",
                        "game_id": "76561198026378636",
                        "game_name": "nationwide13",
                        "game_skill_level": 3,
                        "membership": "",
                        "anticheat_required": false
                    },
                    {
                        "id": "c8b31b95-e86b-4985-8bbf-e2c14b8b8962",
                        "nickname": "SlowBanana",
                        "avatar": "https://assets.faceit-cdn.net/avatars/c8b31b95-e86b-4985-8bbf-e2c14b8b8962_1559825747524.jpg",
                        "game_id": "76561198089419490",
                        "game_name": "SlowBanana",
                        "game_skill_level": 3,
                        "membership": "",
                        "anticheat_required": false
                    }
                ],
                "substitutions": 0,
                "substitutes": null
            }
        ],
        "created_at": "2019-06-12T20:58:40Z",
        "updated_at": "2019-06-12T20:59:51Z"
    }
};
const sampleMatchFinishHook = {
    "transaction_id": "c51ef798-60e3-4fa1-8027-4c3d8cfc1cb8",
    "event": "match_status_finished",
    "event_id": "c6503722-6816-42ea-bc91-7627c6612101",
    "third_party_id": "94607d1b-7fcf-4db7-a085-50fe8c24f2f2",
    "app_id": "11ad71cf-6172-4ec1-9555-e91163264284",
    "timestamp": "2019-06-12T21:40:02Z",
    "retry_count": 0,
    "version": 1,
    "payload": {
        "id": "1-7457f67f-f4d5-4ae1-be64-15abfbb884eb",
        "organizer_id": "faceit",
        "region": "NA",
        "game": "battalion",
        "version": 42,
        "entity": {
            "id": "36c6f816-f417-4477-9afb-f67dec946191",
            "name": "5v5 WARTIDE",
            "type": "matchmaking"
        },
        "teams": [
            {
                "id": "edc24b80-1b89-4895-b5ed-08c2c4166d17",
                "name": "team_Jasonm31178",
                "type": "",
                "avatar": "",
                "leader_id": "edc24b80-1b89-4895-b5ed-08c2c4166d17",
                "co_leader_id": "",
                "roster": [
                    {
                        "id": "b458f71e-0c65-4040-b0c7-fd5d84f60103",
                        "nickname": "Draaaake",
                        "avatar": "",
                        "game_id": "76561198091687678",
                        "game_name": "Draaaake",
                        "game_skill_level": 3,
                        "membership": "",
                        "anticheat_required": false
                    },
                    {
                        "id": "1e1db879-e9f5-4ada-90ed-66cf7b1e2ab8",
                        "nickname": "TheMagic",
                        "avatar": "https://assets.faceit-cdn.net/avatars/1e1db879-e9f5-4ada-90ed-66cf7b1e2ab8_1553472718614.jpg",
                        "game_id": "76561198036484203",
                        "game_name": "TheMagic",
                        "game_skill_level": 3,
                        "membership": "",
                        "anticheat_required": false
                    },
                    {
                        "id": "137403c6-cb61-46ab-a36e-8be0cb1cf53c",
                        "nickname": "Snorplet",
                        "avatar": "",
                        "game_id": "76561198835785075",
                        "game_name": "Snorplet",
                        "game_skill_level": 3,
                        "membership": "",
                        "anticheat_required": false
                    },
                    {
                        "id": "47552eb2-9d81-489e-854f-726f3f0daec2",
                        "nickname": "GonRL",
                        "avatar": "https://assets.faceit-cdn.net/avatars/47552eb2-9d81-489e-854f-726f3f0daec2_1560303255466.jpg",
                        "game_id": "76561198266490093",
                        "game_name": "GonRL",
                        "game_skill_level": 3,
                        "membership": "",
                        "anticheat_required": false
                    },
                    {
                        "id": "edc24b80-1b89-4895-b5ed-08c2c4166d17",
                        "nickname": "Jasonm31178",
                        "avatar": "",
                        "game_id": "76561198137252986",
                        "game_name": "Jasonm31178",
                        "game_skill_level": 3,
                        "membership": "",
                        "anticheat_required": false
                    }
                ],
                "substitutions": 0,
                "substitutes": null
            },
            {
                "id": "c8b31b95-e86b-4985-8bbf-e2c14b8b8962",
                "name": "team_SlowBanana",
                "type": "",
                "avatar": "https://assets.faceit-cdn.net/avatars/c8b31b95-e86b-4985-8bbf-e2c14b8b8962_1559825747524.jpg",
                "leader_id": "c8b31b95-e86b-4985-8bbf-e2c14b8b8962",
                "co_leader_id": "",
                "roster": [
                    {
                        "id": "d6ab8dd4-442f-4c18-bb5e-c3704348db69",
                        "nickname": "BAMBAM15435",
                        "avatar": "",
                        "game_id": "76561198350193474",
                        "game_name": "BAMBAM15435",
                        "game_skill_level": 3,
                        "membership": "",
                        "anticheat_required": false
                    },
                    {
                        "id": "abbc8f50-897d-41f0-9a5d-1fac530e955d",
                        "nickname": "NitrousTV",
                        "avatar": "https://assets.faceit-cdn.net/avatars/abbc8f50-897d-41f0-9a5d-1fac530e955d_1550834631821.jpg",
                        "game_id": "76561198079633804",
                        "game_name": "NitrousTV",
                        "game_skill_level": 3,
                        "membership": "",
                        "anticheat_required": false
                    },
                    {
                        "id": "20b73b21-55dd-4f48-bb59-0b7436630f17",
                        "nickname": "MeoBoi",
                        "avatar": "",
                        "game_id": "76561198001474191",
                        "game_name": "MeoBoi",
                        "game_skill_level": 3,
                        "membership": "",
                        "anticheat_required": false
                    },
                    {
                        "id": "94607d1b-7fcf-4db7-a085-50fe8c24f2f2",
                        "nickname": "nationwide13",
                        "avatar": "",
                        "game_id": "76561198026378636",
                        "game_name": "nationwide13",
                        "game_skill_level": 3,
                        "membership": "",
                        "anticheat_required": false
                    },
                    {
                        "id": "c8b31b95-e86b-4985-8bbf-e2c14b8b8962",
                        "nickname": "SlowBanana",
                        "avatar": "https://assets.faceit-cdn.net/avatars/c8b31b95-e86b-4985-8bbf-e2c14b8b8962_1559825747524.jpg",
                        "game_id": "76561198089419490",
                        "game_name": "SlowBanana",
                        "game_skill_level": 3,
                        "membership": "",
                        "anticheat_required": false
                    }
                ],
                "substitutions": 0,
                "substitutes": null
            }
        ],
        "created_at": "2019-06-12T20:58:40Z",
        "updated_at": "2019-06-12T21:40:02Z",
        "started_at": "2019-06-12T21:00:58Z",
        "finished_at": "2019-06-12T21:40:02Z"
    }
};
const sampleMatchCanceled = {
    "transaction_id": "d1cec913-e321-4fd3-8e78-3bbf47f4921e",
    "event": "match_status_cancelled",
    "event_id": "291c430b-de22-4173-95e4-e49ab5d0f39a",
    "third_party_id": "94607d1b-7fcf-4db7-a085-50fe8c24f2f2",
    "app_id": "11ad71cf-6172-4ec1-9555-e91163264284",
    "timestamp": "2019-06-14T16:01:17Z",
    "retry_count": 0,
    "version": 1,
    "payload": {
        "id": "1-efd12f68-d0e5-43af-a2ce-0db4b89b4a0d",
        "organizer_id": "faceit",
        "region": "NA",
        "game": "battalion",
        "version": 17,
        "reason": "AFK",
        "entity": {
            "id": "36c6f816-f417-4477-9afb-f67dec946191",
            "name": "5v5 WARTIDE",
            "type": "matchmaking"
        },
        "teams": [
            {
                "id": "94607d1b-7fcf-4db7-a085-50fe8c24f2f2",
                "name": "team_nationwide13",
                "type": "",
                "avatar": "",
                "leader_id": "94607d1b-7fcf-4db7-a085-50fe8c24f2f2",
                "co_leader_id": "",
                "roster": [
                    {
                        "id": "94607d1b-7fcf-4db7-a085-50fe8c24f2f2",
                        "nickname": "nationwide13",
                        "avatar": "",
                        "game_id": "76561198026378636",
                        "game_name": "nationwide13",
                        "game_skill_level": 3,
                        "membership": "",
                        "anticheat_required": false
                    },
                    {
                        "id": "2c924752-9877-482e-b763-da22d5131148",
                        "nickname": "K1ngMaurer",
                        "avatar": "",
                        "game_id": "76561198130848439",
                        "game_name": "K1ngMaurer",
                        "game_skill_level": 2,
                        "membership": "",
                        "anticheat_required": false
                    },
                    {
                        "id": "55fc6343-573a-4eac-b036-3fc3f0553e26",
                        "nickname": "Amishx",
                        "avatar": "",
                        "game_id": "76561198018831275",
                        "game_name": "Amishx",
                        "game_skill_level": 3,
                        "membership": "",
                        "anticheat_required": false
                    },
                    {
                        "id": "800ec801-dba5-42d2-a427-44bcf3edb264",
                        "nickname": "TotalApollo",
                        "avatar": "https://assets.faceit-cdn.net/avatars/800ec801-dba5-42d2-a427-44bcf3edb264_1550874579592.jpg",
                        "game_id": "76561198171406977",
                        "game_name": "TotalApollo",
                        "game_skill_level": 4,
                        "membership": "",
                        "anticheat_required": false
                    },
                    {
                        "id": "a1622a70-7d62-430c-8f2d-d4eead156909",
                        "nickname": "DemonicRage",
                        "avatar": "https://assets.faceit-cdn.net/avatars/a1622a70-7d62-430c-8f2d-d4eead156909_1550877511861.jpg",
                        "game_id": "76561198218140847",
                        "game_name": "DemonicRage",
                        "game_skill_level": 2,
                        "membership": "",
                        "anticheat_required": false
                    }
                ],
                "substitutions": 0,
                "substitutes": null
            },
            {
                "id": "339fa777-92ac-4951-980c-3a058e1f7a4f",
                "name": "team_iApolloo",
                "type": "",
                "avatar": "https://assets.faceit-cdn.net/avatars/339fa777-92ac-4951-980c-3a058e1f7a4f_1550889726222.png",
                "leader_id": "339fa777-92ac-4951-980c-3a058e1f7a4f",
                "co_leader_id": "",
                "roster": [
                    {
                        "id": "5b0cb31d-d66a-404a-8e77-063456e20daf",
                        "nickname": "kudoku",
                        "avatar": "",
                        "game_id": "76561197966429290",
                        "game_name": "kudoku",
                        "game_skill_level": 3,
                        "membership": "",
                        "anticheat_required": false
                    },
                    {
                        "id": "339fa777-92ac-4951-980c-3a058e1f7a4f",
                        "nickname": "iApolloo",
                        "avatar": "https://assets.faceit-cdn.net/avatars/339fa777-92ac-4951-980c-3a058e1f7a4f_1550889726222.png",
                        "game_id": "76561198141230080",
                        "game_name": "iApolloo",
                        "game_skill_level": 3,
                        "membership": "",
                        "anticheat_required": false
                    },
                    {
                        "id": "27ff1294-fc0f-4858-880c-a0f2fbc30c41",
                        "nickname": "static144",
                        "avatar": "https://assets.faceit-cdn.net/avatars/27ff1294-fc0f-4858-880c-a0f2fbc30c41_1560101518593.jpg",
                        "game_id": "76561197960419815",
                        "game_name": "static144",
                        "game_skill_level": 2,
                        "membership": "",
                        "anticheat_required": false
                    },
                    {
                        "id": "6e924f1e-c4ff-4bf4-8812-8544e4191da4",
                        "nickname": "AmazingJoo",
                        "avatar": "",
                        "game_id": "76561198154055230",
                        "game_name": "AmazingJoo",
                        "game_skill_level": 3,
                        "membership": "",
                        "anticheat_required": false
                    },
                    {
                        "id": "7e83de78-8c77-4284-989b-adec30ac549e",
                        "nickname": "kingreapftw",
                        "avatar": "https://assets.faceit-cdn.net/avatars/7e83de78-8c77-4284-989b-adec30ac549e_1550791020938.jpg",
                        "game_id": "76561198151129127",
                        "game_name": "kingreapftw",
                        "game_skill_level": 3,
                        "membership": "",
                        "anticheat_required": false
                    }
                ],
                "substitutions": 0,
                "substitutes": null
            }
        ],
        "created_at": "2019-06-14T15:56:01Z",
        "updated_at": "2019-06-14T16:01:17Z"
    }
}
